import React from 'react';
import { MdRemoveCircleOutline, MdAddCircleOutline, MdDelete } from 'react-icons/md';

import { Container, ProductTable, Total } from './styles';

export default function Cart() {
  return (
  <Container>
    <ProductTable>
      <thead>
        <tr>
          <th />
          <th>PRODUTO</th>
          <th>QTD</th>
          <th>SUBTOTAL</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <img src="https://static.netshoes.com.br/produtos/tenis-nike-revolution-5-masculino/26/HZM-1731-026/HZM-1731-026_zoom2.jpg?ts=1610983830&ims=326x"
              alt="Tênis"
            />
          </td>
          <td>
            <strong> Tênis Nike</strong>
            <span> R$129,90</span>
            <td>
              <div>
                <button type="button">
                  <MdRemoveCircleOutline size={20} color="#7159c1" />
                </button>
                <input type="number" readOnly value={1}/>
                <button type="button">
                  <MdAddCircleOutline size={20} color="#7159c1" />
                </button>
              </div>
            </td>
          </td>
          <td>
            <strong>R$258,80</strong>
          </td>
          <td>
            <button type="button">
              <MdDelete size={20} color="#7159c1" />
            </button>
          </td>
        </tr>
      </tbody>
    </ProductTable>

    <footer>
      <button type="button">Finalizar pedido </button>
      <Total>
        <span>TOTAL</span>
        <strong> R$129.90</strong>
      </Total>
    </footer>
  </Container>

  );
}

import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.header`
  display: flex;
  justify-content: space-between; // para um item ficar ao lado do outro
  align-items: center; // se um intem for maior q o outro, os dois ficam alinhados ao centro
  margin: 50px 0;
`;

export const Cart = styled(Link)`
  display: flex;
  align-items: center;
  text-decoration: none; // tira o sublinhado do link
  transition: opacity: 0.2s;

  &:hover {
    opacity: 0.7;
  }

  div {
    text-align: right;
    margin-right: 10px;
  }

  strong {
    display: block;
    color: #FFF;
  }

  span {
    font-size: 12px;
    color: #999;
  }

`;
